const {
    check,
    validationResult
} = require('express-validator/check');

validators = {
    loginValidator: [
        check('username').not().isEmpty(),
        check('password').not().isEmpty()
    ],
    registerValidator: [
        check('username').not().isEmpty(),
        check('password').not().isEmpty(),
        check('password','password min length is 8').custom((value)=> value.length >= 8),
        check('confirmpassword', 'password and confirmpassword must match').custom((value, {
            req
        }) => 
            value === req.body.password
        )
    ]
}

exports.validate = (validatorName) => {
    return [validators[validatorName], (req, res, next) => {
        let valE = validationResult(req);
        if (!valE.isEmpty()) {
            return res.status(422).json({
                errors: valE.array()
            });
        }
        next();
    }]
}