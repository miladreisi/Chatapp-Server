const socketio = require('socket.io');
const socketOptions = {
    pingTimeout: 1000,
    pingInterval: 20000
}
exports.init = (server) => {
    const io = new socketio(server, socketOptions);
    setupListeners(io);
}

function setupListeners(io) {
    io.on('connection', (socket) => {
        console.log('new connection');

        socket.on('message', (data) => {
            console.log(`socket message event`)
            console.log(data)
            io.emit('message', data)
        })
    });
}