const jwt = require('jsonwebtoken');
const tokenSecret = 'mysecret';

exports.generateToken = (payload) => {
    let token = jwt.sign(payload, tokenSecret);
    return token;
}

exports.validateToken = validateToken = (token) => {
    try {
        let decoded = jwt.verify(token, tokenSecret);
        return decoded;
    } catch (error) {
        console.log(error);
        return false;
    }
}

exports.validateTokenMid = (req, res, next) => {
    let validToken = validateToken(req.headers.token);
    if (validToken !== false) {
        req.decodedToken = validToken;
        next();
        return;
    }
    return res.status(401).send('unauthorized');
}