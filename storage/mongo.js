const mongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
const dbName = 'testDB';

let db = null;

mongoClient.connect(url, (err, client) => {
    if (err) {
        console.log(err);
    } else {
        console.log('connected to mongodb successfully');
        db = client.db(dbName);
    }
});


module.exports.login = async (username, password) => {
    let user = await db.collection('user').find({
        'username': username,
        'password': password
    }).limit(1).toArray();

    return user.length == 1;
}

module.exports.userExists = async (username) => {
    let user = await db.collection('user').find({
        'username': username
    }).limit(1).toArray();

    return user.length == 1;
}

module.exports.register = async (username, password) => {
    let r = await db.collection('user').insertOne({
        username,
        password
    });
    return r.insertedCount == 1;
}