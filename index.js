const express = require('express');
const validators = require('./validators/index');
const db = require('./storage/mongo');
const authenticator = require('./authenticators/jwt');
const http = require('http');
const comminicution = require('./comminicution/socketio');

const app = express();
const server = http.createServer(app);
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.post('/register', validators.validate('registerValidator'), async (req, res) => {
    let userExists = await db.userExists(req.body.username);
    if (userExists) {
        return res.json({
            err: 'userExists'
        });
    }

    let regToDB = await db.register(req.body.username, req.body.password);
    if (regToDB) {
        res.json({
            msg: 'ok'
        });
    } else {
        res.json({
            err: 'regError'
        });
    }
});

app.post('/login', validators.validate('loginValidator'), async (req, res) => {
    let userExists = await db.login(req.body.username, req.body.password);
    if (!userExists) {
        return res.status(401).send('invalidCredintial');
    }

    let token = authenticator.generateToken({
        username: req.body.username
    });

    return res.send(token);
});

app.get('/message', authenticator.validateTokenMid, (req, res) => {
    res.json(req.decodedToken.username);
});




// app.listen(port, () => {
//     console.log(`server is listening on port ${port}`);
// });
server.listen(port);
comminicution.init(server);